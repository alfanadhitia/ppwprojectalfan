from django.test import TestCase
import os
from time import sleep
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from project.settings import BASE_DIR
from story9.views import login_view, logout_view, signup_view
from django.core.management import call_command

# Create your tests here.
class Story9Test(TestCase):
    def setUp(self):
        user = User.objects.create_user(username="samlekom",password="alfanganteng")
        user.save()

    def test_url_login_is_exist(self):
        response = self.client.get('/story9/login/')
        self.assertEqual(response.status_code,200)

    def test_url_logout_is_exist(self):
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code,200)

    def test_url_signup_is_exist(self):
        response = self.client.get('/story9/signup/')
        self.assertEqual(response.status_code,200)
    
    def test_url_does_not_exist(self):
        response = self.client.get('/story9/wek/')
        self.assertEqual(response.status_code,404)

    def test_using_correct_signin_template(self):
        response = self.client.get('/story9/login/')
        self.assertTemplateUsed(response,'login.html')

    def test_using_correct_signup_template(self):
        response = self.client.get('/story9/signup/')
        self.assertTemplateUsed(response,'signup.html')
    
    def test_using_function_login_views(self):
        response = resolve('/story9/login/')
        self.assertEqual(response.func,login_view)

    def test_using_function_logout_view(self):
        response = resolve('/story9/logout/')
        self.assertEqual(response.func,logout_view)

    def test_using_function_signup_view(self):
        response = resolve('/story9/signup/')
        self.assertEqual(response.func,signup_view)

    def test_signup_add_user(self):
        response = self.client.post('/story9/signup/',data={
            'username':"InsyallahPPWA",
            'password1':'mantap12345',
            'password2': 'mantap12345',
        })
        user = User.objects.all()
        self.assertEqual(len(user),2)
        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code,302)
        self.assertRedirects(expected_url='/buku/',response=response)

    def test_signup_fail_password(self):
        response = self.client.post('/story9/signup/',data={
            'username':"salahmen",
            'password1':'1234567',
            'password2': '1234567',
        })
        user = get_user_model().objects.all()

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(len(user),1)
        self.assertEqual(response.status_code,200)

    def test_login(self):
        response = self.client.post('/story9/login/',data={
            'username':"samlekom",
            'password':'alfanganteng',
        })

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/buku/',response=response)

    def test_already_login(self):
        response = self.client.post('/story9/login/',data={
            'username':"samlekom",
            'password':'alfanganteng',
        })

        response = self.client.get('/story9/login/')

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/buku/',response=response)

        response = self.client.get('/story9/signup/')

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/buku/',response=response)


    def test_login_fail_no_user(self):
        response = self.client.post('/story9/login/',data={
            'username':'test',
            'password':'123412341234'
        })

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)

    def test_signout(self):
        response = self.client.post('/story9/login/',data={
            'username':"samlekom",
            'password':'alfanganteng',
        })

        response = self.client.post('/story9/logout/')

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/buku/',response=response , target_status_code=302)


class LiveTestCase(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        call_command('collectstatic', verbosity=0, interactive=False)
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        user = User.objects.create_user(username="samlekom",password="alfanganteng")
        user.save()
        super(LiveTestCase,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(LiveTestCase,self).tearDown()

    def test_signin_required(self):
        browser = self.browser
        browser.get(self.live_server_url+"/buku/")

        next = browser.find_element_by_name('next')
        self.assertEqual('Sign In', browser.title)
        self.assertEqual('/buku/',next.get_property('value'))

        form_username = browser.find_element_by_id('id_username')
        form_password = browser.find_element_by_id('id_password')
        submit_btn = browser.find_element_by_name('login-btn')

        form_username.send_keys('samlekom')
        form_password.send_keys('alfanganteng')

        submit_btn.click()

        self.assertEqual('Story 8',browser.title)

    def test_signout(self):
        browser = self.browser
        browser.get(self.live_server_url+"/buku/")

        form_username = browser.find_element_by_id('id_username')
        form_password = browser.find_element_by_id('id_password')
        submit_btn = browser.find_element_by_name('login-btn')

        form_username.send_keys('samlekom')
        form_password.send_keys('alfanganteng')


        submit_btn.click()

        btn_logout = browser.find_element_by_name('logout-btn')
        btn_logout.click()

        self.assertEqual('Sign In',browser.title)
        self.assertNotIn('_auth_user_id', self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
