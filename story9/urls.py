from django.urls import path
from story9.views import signup_view,login_view,logout_view

app_name = "story9"
urlpatterns = [
    path('signup/', signup_view , name='signup'),
    path('login/',login_view , name= 'login'),
    path('logout/',logout_view , name= 'logout'),
]