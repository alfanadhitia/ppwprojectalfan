from django.test import TestCase, Client
from . import views
import json
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options
import time
import unittest
from django.contrib.staticfiles.testing import StaticLiveServerTestCase, LiveServerTestCase
from django.urls import reverse
from project.settings import BASE_DIR
import os
from django.core.management import call_command
from django.contrib.auth.models import User


class Story8Test(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')

    def test_url_does_not_exist(self):
        response = self.client.get("/wek/")
        self.assertEqual(response.status_code,404)

    def test_url_is_exist(self):
        response = self.client.get("/buku/")
        self.assertEqual(response.status_code,200)

    def test_page_using_correct_function(self):
        response = resolve("/buku/")
        self.assertEqual(response.func,views.daftar_buku)
    
    def test_page_using_tembak_API_AJAX(self):
        response = resolve("/buku/daftar_buku/test")
        self.assertEqual(response.func,views.get_JSON_daftar_buku)

    def test_using_correct_template(self):
        response = self.client.get("/buku/")
        self.assertTemplateUsed(response,"daftar_buku.html")

    def test_response_JSON_AJAX(self):
        response = self.client.get("/buku/daftar_buku/test")
        self.assertEqual(type(json.loads(response.content)),dict)

class TestProjectFunctional (LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(TestProjectFunctional,self).setUp()
        call_command('collectstatic', verbosity=0, interactive=False)
        test_user = User.objects.create(username='Testuser')
        test_user.set_password('senha8dg')
        test_user.save()
        # Login the user
        self.assertTrue(self.client.login(username='Testuser', password='senha8dg'))
        # Add cookie to log in the browser
        cookie = self.client.cookies['sessionid']
        self.selenium.get(self.live_server_url) # visit page in the site domain so the page accepts the cookie
        self.selenium.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})

    def tearDown(self):
        self.selenium.quit()
        super(TestProjectFunctional,self).tearDown()

    def test_title_is_correct(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/buku/")
        self.assertIn("Story 8",selenium.title)

    def test_header_is_correct (self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/buku/")
        header = selenium.find_element_by_id('header').text
        self.assertEquals('Find Your Books', header)

    def test_default_search_is_correct(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/buku/")
        judul_test = "Systematic Software Testing"
        time.sleep(10)
        self.assertIn(judul_test,selenium.page_source)






