from django.contrib import admin
from django.urls import path,include
from story8.views import daftar_buku,get_JSON_daftar_buku

app_name = 'story8'

urlpatterns = [
    path('', daftar_buku , name='buku'),
    path('daftar_buku/<str:value>' , get_JSON_daftar_buku , name = "daftar_buku")
]
