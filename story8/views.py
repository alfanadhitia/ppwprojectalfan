from django.shortcuts import render
from django.http import JsonResponse
import urllib.request, json
import requests
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required(login_url='/story9/login/')
def daftar_buku(request):
    return render(request,"daftar_buku.html",{"name": request.user.username })

def get_JSON_daftar_buku(request,value):
    req = requests.get("https://www.googleapis.com/books/v1/volumes?q="+value)
    data = req.json()
    return JsonResponse(data)