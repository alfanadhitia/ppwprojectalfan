function cariBuku(){
    var search = $('#search').val()
    console.log(search)
    var data_buku = ""

    $.ajax({
        url: "/buku/daftar_buku/"+search,
        dataType: "json",
        error: function(){
            alert("Something is Error , Please try again")
        }
        ,
        success: function(data){
            console.log(data)
            $('#buku').empty()
            if(typeof (data.items) === 'undefined'){
                alert("There are no books in this keyword , please find another keyword");
            }

            for( var i = 0 ; i<data.items.length ; i++){
                data_buku += '<tr>';
                data_buku += '<td>' +  data.items[i].volumeInfo.title + '</td>';
                if( typeof (data.items[i].volumeInfo.authors) === 'undefined' ){
                    data_buku += "<td> No Author  </td>";   
                }else{
                    data_buku += '<td>' +  data.items[i].volumeInfo.authors + '</td>';
                }
                if( (typeof(data.items[i].volumeInfo.imageLinks)) === 'undefined' ){
                    data_buku += "<td> No Thumbnail  </td>";   
                }else{
                    data_buku += '<td>' + '<img src=' + data.items[i].volumeInfo.imageLinks.thumbnail + "> </td>";
                }
                data_buku += '</tr>';

            }
            console.log(data_buku)
            $("#buku").append(data_buku)
        },

    });
}

$(document).ready(defaultCariBuku)

function defaultCariBuku(){
    var data_buku_default=''
    $.ajax({
        url: "/buku/daftar_buku/testing",
        dataType: "json",
        success: function(data){
            console.log(data)
            $('#buku').empty()
            for( var i = 0 ; i<data.items.length ; i++){
                data_buku_default += '<tr>';
                data_buku_default += '<td>' +  data.items[i].volumeInfo.title + '</td>';
                if( typeof (data.items[i].volumeInfo.authors) === 'undefined' ){
                    data_buku_default += "<td> No Author  </td>";   
                }else{
                    data_buku_default += '<td>' +  data.items[i].volumeInfo.authors + '</td>';
                }
                if( (typeof(data.items[i].volumeInfo.imageLinks)) === 'undefined' ){
                    data_buku_default += "<td> No Thumbnail  </td>";   
                }else{
                    data_buku_default += '<td>' + '<img src=' + data.items[i].volumeInfo.imageLinks.thumbnail + "> </td>";
                }
                data_buku_default += '</tr>';

            }
            console.log(data_buku_default)
            $("#buku").append(data_buku_default)
        },
    });
}