from django.test import TestCase
from django.test import Client
from Status.views import index
from Status.models import StatusModel
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.chrome.options import Options
import time
import unittest
from django.contrib.staticfiles.testing import LiveServerTestCase
from django.urls import reverse
from project.settings import BASE_DIR
import os
from django.core.management import call_command

class TestProjectFunctional (LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(TestProjectFunctional,self).setUp()
        call_command('collectstatic', verbosity=0, interactive=False)
        print("Browser On")

    def tearDown(self):
        self.selenium.quit()
        super(TestProjectFunctional,self).tearDown()
        print("browser off")

    def test_opening_browser_user_see_title(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        self.assertIn("Status",selenium.title)
        print("Judulnya Sesuai")

    def test_user_sees_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        header = selenium.find_element_by_name('header-utama').text
        self.assertEquals('Halo,Apa Kabar?', header)
        print("User Melihat Header")

    def test_inputting_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        message = "Test"
        self.assertNotIn(message,selenium.page_source)
        text_area = selenium.find_element_by_name("status_desc")
        text_area.send_keys(message)
        sub_button = selenium.find_element_by_id("postbutton")
        sub_button.click()
        self.assertIn(message,selenium.page_source)
        print("Kata Test Muncul di Body")

    # 1 rem = 16px
    def test_ukuran_header(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        text = selenium.find_element_by_name("header-utama").value_of_css_property("font-size")
        self.assertEquals('40px',text)
        print("Ukuran Judul sesuai")

    def test_ada_button_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol = "<button"
        self.assertIn(tombol,selenium.page_source)
        print("ada tombol post")
    
    def test_ada_input_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        kolom_status = "<textarea"
        self.assertIn(kolom_status,selenium.page_source)
        print("ada kolom status")

    def test_ada_tombol_ganti_tema(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol_tema = "switch"
        self.assertIn(tombol_tema,selenium.page_source)
        print("ada tombol ubah tema")

    def test_fungsional_tombol_tema_dalam_keadaan_terang(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol_tema = selenium.find_element_by_class_name("switch")
        tombol_tema.click()
        time.sleep(5)
        background_web =  selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEquals('rgba(51, 48, 48, 1)', background_web)
        print("Tombol Tema berfungsi dalam keadaan terang")
    
    def test_fungsional_tombol_tema_dalam_keadaan_gelap(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        tombol_tema = selenium.find_element_by_class_name("switch")
        tombol_tema.click()
        time.sleep(5)
        tombol_tema.click()
        time.sleep(5)
        background =  selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
        self.assertEquals('rgba(252, 252, 252, 1)', background)
        print("Tombol Tema berfungsi dalam keadaan gelap")
    
    def test_accordion_ada(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_ada = selenium.find_element_by_id("jQuery-accordion")
        self.assertIsNotNone(accordion_ada)
        print("accordion sudah ada dan terlihat")
    
    def test_header_accordion_benar(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_header = selenium.find_element_by_name("accordion-header-1").text
        self.assertEqual("Aktivitas",accordion_header)
        print("accordion header sudah benar")

    def test_content_accordion_benar(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_1 = selenium.find_element_by_id('ui-id-1') 
        accordion_1.click()
        accordion_content = selenium.find_element_by_name('content-accordion-1').text
        self.assertIn("VPIC Seminar Educare",accordion_content)

    def test_accordion_awalnya_ketutup(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        time.sleep(3)
        accordion = selenium.find_element_by_id("ui-id-2").value_of_css_property("display")
        self.assertEqual("none" , accordion)
    
    def test_accordion_buka_lihat_konten(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_1 = selenium.find_element_by_id('ui-id-1') 
        accordion_1.click()
        time.sleep(3)
        accordion_content= selenium.find_element_by_id("ui-id-2").value_of_css_property("display")
        self.assertEqual("block" , accordion_content)

    def test_accordion_bisa_ketutup_lagi_pas_dibuka(self):
        selenium = self.selenium
        selenium.get(self.live_server_url+"/")
        accordion_1 = selenium.find_element_by_id('ui-id-1') 
        accordion_1.click()
        time.sleep(3)
        accordion_1.click()
        time.sleep(3)
        accordion_content= selenium.find_element_by_id("ui-id-2").value_of_css_property("display")
        self.assertEqual("none" , accordion_content)


class TestStatus(TestCase):

    def test_status_is_exist(self):
        self.client = Client()
        response = self.client.get('')
        self.assertEqual(response.status_code,200)
    
    def test_status_is_not_exist(self):
        self.client = Client()
        response = self.client.get('wek')
        self.assertEqual(response.status_code,404)

    def test_statustemplate_is_correct (self):
        self.client = Client()
        response = self.client.get('')
        self.assertTemplateUsed(response, 'status.html')

    def test_form(self):
        data = {
            'status_desc' : "Nyante",
            'waktu' : "2019-01-01 00:00:00",
        }
        response = Client().post("", data)
        self.assertEqual(response.status_code, 302)
    
    def test_model_status(self):
        status_test = StatusModel(
            status_desc = "Nyante",
            waktu = "2019-01-01 00:00gi:00",
        ) 
        status_test.save()
        self.assertEqual(StatusModel.objects.all().count(), 1)


