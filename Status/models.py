from django.db import models
import django.utils.timezone

# Create your models here.

class StatusModel(models.Model):
    status_desc = models.CharField(max_length=300)
    waktu = models.DateTimeField(auto_now=True , blank=True)